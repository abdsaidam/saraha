package com.example.saraha.adapter.recycle_comment;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.saraha.LocalDatabase.UserPref;
import com.example.saraha.R;
import com.example.saraha.adapter.adapter_saerch.holder_of_resycle;

import com.example.saraha.adapter.story_recycle.stories_adapter;
import com.example.saraha.api.RetrofitSettings;
import com.example.saraha.models.CommentModelResc;
import com.example.saraha.models.Message;
import com.example.saraha.models.hodel_of_search_resc;
import com.example.saraha.models.replaymassagemodel;
import com.example.saraha.views.Edit_profile_actievity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class comment_adapter extends RecyclerView.Adapter<holder_comment> {
    private ArrayList<Message> arraycomment = new ArrayList<>();
    public holder_comment hoh;
    private RecyclerViewClickListener mListener;
    View view;


    private Context context;

    public comment_adapter(Context context1, ArrayList<Message> arraycomment) {
        this.arraycomment = arraycomment;
        this.context = context1;
    }

    public interface RecyclerViewClickListener {

        void onClick(int position);
    }


    @NonNull
    @Override
    public holder_comment onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_comment_item, parent, false);
        return new holder_comment(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final holder_comment holder, final int position) {


        Log.d("index", "" + position);
        holder.setData(arraycomment.get(position));
       // final int posi = holder.getAdapterPosition();
      //  final Intent it=new Intent(context, Edit_profile_actievity.class);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.replay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.textView3.setVisibility(View.VISIBLE);
                        holder.editText1.setVisibility(View.VISIBLE);

                     //   Toast.makeText(context, "" + arraycomment.get(position).getID(), Toast.LENGTH_SHORT).show();

//                        Toast.makeText(context, "fffffffffffffffffff", Toast.LENGTH_SHORT).show();
                        holder.textView3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                replaymethode(holder.editText1 , ""+arraycomment.get(position).getID());
                                holder.textView3.setVisibility(View.GONE);
                                holder.editText1.setVisibility(View.GONE);
                            }
                        });

                    }
                });


            }
        });


    }


    @Override
    public int getItemCount() {

        return arraycomment.size();


    }


    public void replaymethode(EditText editText1 , String idmassage ) {
        String replaytxt = editText1.getText().toString().trim();
        if (replaytxt.isEmpty()) {
            editText1.setError("text is required");
            editText1.requestFocus();
            return;

        }
        UserPref userPref = new UserPref(context);

        String token = userPref.getName();
//        String idmassage = userPref.getmassage();
        Call<replaymassagemodel> call = RetrofitSettings.getInstance().getrequest().addmassagereplay(token, replaytxt, "", "", "", "", idmassage);
        call.enqueue(new Callback<replaymassagemodel>() {
            @Override
            public void onResponse(Call<replaymassagemodel> call, Response<replaymassagemodel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "sucsess" , Toast.LENGTH_SHORT).show();
                    String Tag = "tag";
                    Log.d(Tag, "hhh" + response);
                } else {
                    Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<replaymassagemodel> call, Throwable t) {

            }
        });


    }

}
