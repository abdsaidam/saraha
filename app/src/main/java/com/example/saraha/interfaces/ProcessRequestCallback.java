package com.example.saraha.interfaces;

public interface ProcessRequestCallback {

    void onSuccess();

    void onFailed();
}
