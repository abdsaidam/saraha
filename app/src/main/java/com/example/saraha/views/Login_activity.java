package com.example.saraha.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saraha.LocalDatabase.UserDbController;
import com.example.saraha.LocalDatabase.UserPref;
import com.example.saraha.R;
import com.example.saraha.api.RetrofitSettings;
import com.example.saraha.api.api_controller;
import com.example.saraha.interfaces.ListRequestCallback;
import com.example.saraha.interfaces.ObjectRequestCallback;
import com.example.saraha.models.Example;
import com.jaeger.library.StatusBarUtil;

import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_activity extends Base_activity  implements View.OnClickListener {
    private TextView textView;
    private EditText editText1;
    String email;
    String password;
    private EditText editText2;
    private Example example;
    private UserDbController userDbController;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideNavigationBar();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);
        StatusBarUtil.setTransparent( this);
        userDbController = new UserDbController(this);
        example=new Example();
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        editText1=findViewById(R.id.email);
        editText2=findViewById(R.id.pass);
        editText1.setOnClickListener(this);
        editText2.setOnClickListener(this);
        textView=findViewById(R.id.forget_passsss);
        button=findViewById(R.id.log_in);

        textView.setOnClickListener(this);
        button.setOnClickListener(this);


    }


    private void userlogin(){
         email=editText1.getText().toString().trim();
         password=editText2.getText().toString().trim();
        if (email.isEmpty()){
            editText1.setError("Email is required");
            editText1.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editText1.setError("Enter Valid email");
            editText1.requestFocus();
            return;
        }
        if (password.isEmpty()){
            editText2.setError("password required");
            editText2.requestFocus();
            return;
        }
        Call<Example>call= RetrofitSettings.getInstance().getrequest().getPostssss(email,password);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                int position=0;
                if (response.isSuccessful()){
                    String id=String.valueOf(response.body().getInfo().get(position).getID());
                    UserPref  userPref= new UserPref(Login_activity.this);
                    userPref.saveUser(response.body().getAccessToken(),response.body().getInfo().get(0).getImage(),response.body().getInfo().get(position).getName(),response.body().getInfo().get(0).getEmail(),id,response.body().getInfo().get(0).getNickName());
                }

                Long asd =userDbController.createUser(example);
                if (asd>0){
                    Toast.makeText(Login_activity.this, "aaaaaajjjjjjaaaaaaaawertya", Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(Login_activity.this, Home_activety.class);
                startActivity(intent);

                Toast.makeText(Login_activity.this, "aaaaaaaaaaaaaaa", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Toast.makeText(Login_activity.this, "zzzzzzzzzz", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forget_passsss:


                break;
            case R.id.log_in:
                userlogin();


                break;



        }}

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);

    }
}

