package com.example.saraha.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.saraha.R;
import com.jaeger.library.StatusBarUtil;

public class welcome extends Base_activity implements View.OnClickListener {
    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideNavigationBar();



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        StatusBarUtil.setTransparent( this);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        button1=findViewById(R.id.buttsign);
        button2=findViewById(R.id.sign_up);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
    }



    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(color, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(color));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sign_up:
                Intent intent = new Intent(this, create_an_account.class);
                startActivity(intent);

                break;
            case R.id.buttsign:
                Intent intent1 = new Intent(this, Login_activity.class);
                startActivity(intent1);

                break;
    }}

    }

