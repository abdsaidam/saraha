package com.example.saraha.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saraha.LocalDatabase.UserPref;
import com.example.saraha.R;
import com.example.saraha.adapter.imge_profile_res.adapter_img_profil;
import com.example.saraha.adapter.story_recycle.stories_adapter;
import com.example.saraha.api.RetrofitSettings;
import com.example.saraha.models.Example;
import com.example.saraha.models.model_of_addmassage;
import com.example.saraha.models.model_of_res_img_profile;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User_profile_actevity extends Base_activity implements View.OnClickListener{
    private ArrayList<model_of_res_img_profile> array1=new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText editText;
    private TextView textView1;
    private TextView textView;
    private TextView name;
    private TextView nikename;
    public String ids;
    private ScrollView scrollView;
    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideNavigationBar();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_actevity);
        StatusBarUtil.setTransparent( this);
        findview();
        setname();
        setdata();
        initializeRecyclerAdapter3();
    }
    private void findview (){
        recyclerView=findViewById(R.id.profile_img_res);
        editText=findViewById(R.id.textttttttt);
        editText.setOnClickListener(this);
        constraintLayout=findViewById(R.id.const1);
        name=findViewById(R.id.txtttt);
        nikename=findViewById(R.id.nikename);
        scrollView=findViewById(R.id.ssssssss);
        constraintLayout.setOnClickListener(this);
        constraintLayout.scrollTo(0,constraintLayout.getScrollY());



        textView=findViewById(R.id.action);
        textView.setOnClickListener(this);
    }
    public void setname(){
        String names=getIntent().getExtras().getString("name");
        String niknames=getIntent().getExtras().getString("nikename");
        name.setText(names.toString());
        nikename.setText(niknames.toString());


    }
    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        adapter_img_profil adapter =new adapter_img_profil(array1);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    private  void setdata(){
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
        array1.add(new model_of_res_img_profile(R.drawable.ic_rc_prof_img));
    }
    public  void  addmassage(){
        ids=getIntent().getExtras().getString("categoryId");
     //   Toast.makeText(User_profile_actevity.this, ""+ids, Toast.LENGTH_SHORT).show();
        String addmassage=editText.getText().toString().trim();
        if (addmassage.isEmpty()) {
            editText.setError("massage is required");
            editText.requestFocus();
            return;
        }
        UserPref userPref=new UserPref(User_profile_actevity.this);

        String s= userPref.getName();
        String id=userPref.getid();
        Call<model_of_addmassage> call= RetrofitSettings.getInstance().getrequest().addmassage(addmassage,ids,s,"","","","");
        call.enqueue(new Callback<model_of_addmassage>() {
            @Override
            public void onResponse(Call<model_of_addmassage> call, Response<model_of_addmassage> response) {
                if (response.isSuccessful()){
                    Toast.makeText(User_profile_actevity.this, "sucsess", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(User_profile_actevity.this, "fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<model_of_addmassage> call, Throwable t) {

            }
        });





    }

    @Override
    public void onClick(View v) {
        if (v==textView){
            addmassage();
        }
        else if (v==editText){
            scrollView.scrollTo(0,scrollView.getBottom());
        }

    }
}
