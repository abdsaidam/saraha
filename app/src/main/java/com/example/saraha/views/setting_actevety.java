package com.example.saraha.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.saraha.Fragement.My_profile;
import com.example.saraha.LocalDatabase.UserPref;
import com.example.saraha.R;
import com.jaeger.library.StatusBarUtil;

public class setting_actevety extends Base_activity implements View.OnClickListener {
    private FrameLayout frameLayout;
    private TextView textView;
    private TextView textView1;
    private TextView languge;
    private TextView logout;
    private TextView contactus;
    private TextView privacy;
    private FragmentManager fragmentManager;
    private TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideNavigationBar();
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent( this);
        setContentView(R.layout.activity_setting_actevety);
        findview();



    }
    private void findview(){
        textView=findViewById(R.id.arrrow);
        textView1=findViewById(R.id.roooooo);
        contactus=findViewById(R.id.contact_us_action);
        logout=findViewById(R.id.table_raw1);
        privacy=findViewById(R.id.privacy_policy);
        contactus.setOnClickListener(this);
        privacy.setOnClickListener(this);
        textView.setOnClickListener(this);
        languge=findViewById(R.id.language);
        languge.setOnClickListener(this);
        logout.setOnClickListener(this);
        textView1.setOnClickListener(this);
        textView2=findViewById(R.id.table_raw);
        textView2.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v==textView1){
            Intent intent = new Intent(this, Edit_profile_actievity.class);
            startActivity(intent);
        }else if (v==textView){
            Intent intent = new Intent(this, Home_activety.class);
            startActivity(intent);
        //    Fragment mFragment = null;
          //  fragmentManager=getSupportFragmentManager();


//            fragmentManager.beginTransaction().replace(R.id.fragments_container, mFragment).commit();
        }
        else if (v==textView2){
            Intent intent = new Intent(this, Forget_pass_activity.class);
            startActivity(intent);

        }
        else if (v==contactus){
            Intent intent = new Intent(this, contact_us_activity.class);
            startActivity(intent);

        }
        else if (v==languge){
            Intent intent = new Intent(this, language_actevity.class);
            startActivity(intent);

        }
        else if (v==privacy){
            getprivacypage();

        }
        else if (v==logout){
            UserPref userPref= new UserPref(this);
            userPref.logout();
            Intent intent = new Intent(this,Login_activity.class);
            startActivity(intent);

        }

        }
        public void getprivacypage(){
            String url="https://sariiih.com/Privacy/";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Home_activety.class);
        startActivity(intent);

    }

}
