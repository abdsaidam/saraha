package com.example.saraha.Fragement;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saraha.R;
import com.example.saraha.adapter.adapter_saerch.search_adapter_recycle;
import com.example.saraha.adapter.recycle_comment.comment_adapter;
import com.example.saraha.api.RetrofitSettings;
import com.example.saraha.models.CommentModelResc;
import com.example.saraha.models.hodel_of_search_resc;
import com.example.saraha.models.holderofarraysearch;
import com.example.saraha.views.comment_actevity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class searchFragment extends Fragment implements View.OnClickListener{
    private ArrayList<holderofarraysearch> arraysearch1=new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView textView;
    private SearchView searchView;
    private View view;
    private FrameLayout fr;


    public searchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       view= inflater.inflate(R.layout.fragment_search, container, false);
        findview();
       return view;
    }
    public void findview (){
        recyclerView =view.findViewById(R.id.recycl_for_search);
        textView=view.findViewById(R.id.rggggg);
        searchView=view.findViewById(R.id.search1);
        textView.setOnClickListener(this);



    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeRecyclerAdapter4();
    }



    @Override
    public void onClick(View v) {

    }
    private void initializeRecyclerAdapter4() {



        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
       getlastregister();

    }
    public void getlastregister(){
        Call<hodel_of_search_resc> call= RetrofitSettings.getInstance().getrequest().getuserlastrigister();
        call.enqueue(new Callback<hodel_of_search_resc>() {
            @Override
            public void onResponse(Call<hodel_of_search_resc> call, Response<hodel_of_search_resc> response) {
                if (response.isSuccessful()){
                    hodel_of_search_resc cos = response.body();
                    arraysearch1 = new ArrayList<>(cos.getInfo());
                    final search_adapter_recycle adapter =new search_adapter_recycle(arraysearch1,getContext());

                    recyclerView.setAdapter(adapter);
                    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                           adapter.getFilter().filter(newText);
                           return true;
                        }

                    });
                   // Toast.makeText(getContext(), ""+arraysearch1.get(0).getID(), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<hodel_of_search_resc> call, Throwable t) {

            }
        });

    }
}
