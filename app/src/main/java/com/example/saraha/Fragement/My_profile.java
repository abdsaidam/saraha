package com.example.saraha.Fragement;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saraha.LocalDatabase.UserPref;
import com.example.saraha.R;
import com.example.saraha.adapter.adaptet_pref_2.adapter_pre;
import com.example.saraha.adapter.recycle_comment.comment_adapter;
import com.example.saraha.adapter.res1_profile_frag.adapter_res1;
import com.example.saraha.adapter.story_recycle.stories_adapter;

import com.example.saraha.api.RetrofitSettings;
import com.example.saraha.models.CommentModelResc;
import com.example.saraha.models.Message;
import com.example.saraha.models.model_of_res_img_profile;
import com.example.saraha.models.model_pref_anather;
import com.example.saraha.models.model_res1_profile_frag;
import com.example.saraha.models.res2_massage_profile_frag;
import com.example.saraha.views.User_profile_actevity;
import com.example.saraha.views.search_actevity;
import com.example.saraha.views.setting_actevety;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class My_profile extends Fragment implements View.OnClickListener {
    public static final String TAG = "pos";
    private ArrayList<model_pref_anather> array1 = new ArrayList<>();
    private CircleImageView imageView;
    private ArrayList<res2_massage_profile_frag> array2 ;
   // ArrayList<Message> carsModels;
    private TextView textView;
    private TextView name;
    private TextView nikename;

    private RecyclerView recyclerView;
    private FrameLayout frameLayout;
    private RecyclerView recyclerView1;
    String namesssss;
    String nikenamessss;

    private int position;
    private View view;
    private FragmentManager fragmentManager;




    public My_profile() {

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        findview();

        return view;
    }

    public void findview() {
        recyclerView = view.findViewById(R.id.res);
        recyclerView1 = view.findViewById(R.id.res1);
        textView=view.findViewById(R.id.ing_icon);
        imageView=view.findViewById(R.id.img_profiles);
        name=view.findViewById(R.id.txtttt);
        nikename=view.findViewById(R.id.tex232);
        textView.setOnClickListener(this);
        frameLayout=view.findViewById(R.id.fragments_container);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getinformation(namesssss,nikenamessss);


       // userPref.Getmobile();

       // Picasso.get().load(userPref.Getmobile()).into(imageView);


        setdata();

        initializeRecyclerAdapter3();
        initializeRecyclerAdapter4();


    }
    public void getinformation(String s1,String s2){
        UserPref  userPref= new UserPref(getContext());
     s1=   userPref.getusername();
     s2=   userPref.getmassage();
     name.setText(s1.toString());
     nikename.setText(s2.toString());




    }

    private void initializeRecyclerAdapter3() {

        recyclerView.setNestedScrollingEnabled(false);
        adapter_pre adapter = new adapter_pre(array1);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setdata() {
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
        array1.add(new model_pref_anather(R.drawable.ic_rc_prof_img));
    }






    private void initializeRecyclerAdapter4() {



        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setHasFixedSize(true);
        getmassageinpublic();



    }


    @Override
    public void onClick(View v) {
        if (v==textView){
            Intent intent = new Intent(getActivity(), setting_actevety.class);
            startActivity(intent);
        }


    }
    public void  getmassageinpublic(){
        UserPref userPref= new UserPref(getContext());
       String id= userPref.getid();
        Call<model_res1_profile_frag> call= RetrofitSettings.getInstance().getrequest().addmassageinpuplic(id);
        call.enqueue(new Callback<model_res1_profile_frag>() {
            @Override
            public void onResponse(Call<model_res1_profile_frag> call, Response<model_res1_profile_frag> response) {
                if (response.isSuccessful()){
                    model_res1_profile_frag cos = response.body();
                    array2 = new ArrayList<>(cos.getMessages());
                    adapter_res1 adapter =new adapter_res1(getContext(),array2);
                    recyclerView1.setAdapter(adapter);

                }

            }

            @Override
            public void onFailure(Call<model_res1_profile_frag> call, Throwable t) {

            }
        });


    }


}