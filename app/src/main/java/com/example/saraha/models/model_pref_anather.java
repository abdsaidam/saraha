package com.example.saraha.models;

public class model_pref_anather {
    private int photo;

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public model_pref_anather(int photo) {
        this.photo = photo;
    }
}
